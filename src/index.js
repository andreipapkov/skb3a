var express = require('express');
var app = express();
var cors = require('cors');

app.use(cors());

const logger = (req, res, next) => {
  console.log(req.url)	;
  next();
}

app.use(logger);

require('es6-promise').polyfill();
require('isomorphic-fetch');

const pcUrl = 'https://gist.githubusercontent.com/isuvorov/ce6b8d87983611482aac89f6d7bc0037/raw/pc.json';


let pc = {};
fetch(pcUrl)
  .then(async (res) => {	  
     pc = await res.json();
  })
  .catch(err => {
     console.log('Чтото пошло не так:', err);
  });

app.get('/task3A/volumes', (req, res) => {
  var result = {};
  (pc.hdd || []).forEach((h) => {
    var r = result[h.volume]|0 + h.size;
    result[h.volume] = r;
  })
  Object.keys(result).forEach(volume => {
    result[volume] += 'B';
  })
  res.json(result);
})



app.get(/^\/task3A\/?(.*)/, (req, res) => {
  var path = (req.params[0] || '');
  var pathArray = path.split(/\//);
  pathArray = pathArray.filter(v => v != '');
  console.log(pathArray);

  var result = pc;
  for (var i = 0; i < pathArray.length; i++){
    var part = pathArray[i];       

   if (typeof result == 'string'){
      result = undefined;
   } else if (Array.isArray(result)){
      result = result[+part];      
   } else {
      result = result[part];
   }

    if (!result){
      break;
    }  
  }
  result !== undefined ? res.json(result) : res.status(404).send('Not Found');
});
	
app.listen(3000, function(){
  console.log('Started!');
});
